# Tahoe-LAFS Mobile coding style

1. Code is much more often read than written.  Take good care and leave the room the way you'd like to find it.
2. Try to make your code so easy to understand, so obvious, as to be boring.
3. Good naming goes a long way.

## General

1. Even if meant for another language (C) we suggest reading [The Power of 10: Rules for Developing Safety-Critical Code](https://web.eecs.umich.edu/~imarkov/10rules.pdf) or, for a summary, the [Wikipedia article](https://en.wikipedia.org/wiki/The_Power_of_10:_Rules_for_Developing_Safety-Critical_Code).
2. Even if meant for another language (C) we suggest reading the [Linux kernel coding style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html).

## Haskell

1. Make sure the code compiles without warnings.
2. We use [hlint](https://github.com/ndmitchell/hlint) incl. a CI job to check for compliance. Our config is in `obelisk/.hlint.yaml`.
3. We use [stylish-haskell](https://github.com/haskell/stylish-haskell) incl. a CI job.  We use the default configuration.
4. We *mostly* follow [Supercede's House Style for Haskell](https://jezenthomas.com/2025/01/style-guide/).

If you should run into the issue that your linting/styling returns code that the CI does not like, it might be a tool version mismatch.
We provide all the tools in the versions we use in a `nix-shell` (inside the `obelisk` directory).

## Markdown

1. We use [Semantic Line Breaks](https://sembr.org/).


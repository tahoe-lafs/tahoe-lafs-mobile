# Interactive click-through prototype

See
https://penpot.gridsync.io/#/view/aa55a301-cee6-8144-8001-e100fcdea40c?page-id=2f5ce419-fdfd-80cc-8001-cb58b83d84cd&section=interactions&index=11&share-id=aa55a301-cee6-8144-8001-e1ec0d2124c6


# Design guidelines

https://developer.android.com/design rightly says

> ### Design for Android
>
> Android users expect your app to look and behave in a way that's consistent with the platform. Not only should you follow material design guidelines for visual and navigation patterns, but you should also follow quality guidelines for compatibility, performance, security, and more.

Here's resources helping us create UI/UX:

- [Android Docs: Core app quality](https://developer.android.com/docs/quality-guidelines/core-app-quality)
  - This has a useful checklist of things one really shouldn't forget.
- [Android Developers' Guide: Principles of navigation](https://developer.android.com/guide/navigation/navigation-principles)
  - This has some good examples for things I'd like to not implement ourselves, but get from Obelisk: The Navigation Stack within our app, the closing of the app when the user presses "back" (but not "up") on our home screen, Deep Linking with Synthetic Back Stacks, etc.
- [Android Guide: Build accessible apps](https://developer.android.com/guide/topics/ui/accessibility)
  - Don't disable handicapped people further by making it impossible for them to use our app.
- [Android Developer Docs: Build for billions](https://developer.android.com/docs/quality-guidelines/build-for-billions)
  - "The markets with the fastest growing internet and smartphone penetration can have some challenging issues" [...] "To help your app succeed and deliver the best possible experience in developing markets, we've put together some advice"

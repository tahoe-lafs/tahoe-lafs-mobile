# Tahoe-LAFS Mobile

[![Most recent pipeline status badge](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/badges/main/pipeline.svg)](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/pipelines?scope=all&ref=main)

![Screenshot 1](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/raw/main/metadata/en-US/images/phoneScreenshots/1.png){width=32%}
![Screenshot 2](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/raw/main/metadata/en-US/images/phoneScreenshots/2.png){width=32%}
![Screenshot 3](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/raw/main/metadata/en-US/images/phoneScreenshots/3.png){width=32%}

## Description

Mobile Tahoe-LAFS client for accessing files stored using [Tahoe-LAFS](https://tahoe-lafs.org),
building on / forking [PrivateStorage Mobile](https://whetstone.private.storage/privatestorage/privatestoragemobile/).

## License

This application is licensed GPL-3.0-only with app-store exception (an additional permission under section 7).
Please see the `LICENSE` file.

## Overall plan

We have [milestones](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/milestones)
and [tickets](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/issues)
for a rough idea of who's working on what, what's completed, and what's blocking.

## Project status

This application is in the early stages of development.
See the issue tracker and the overall plan document for detailed status information.

## Functionality

### Platforms

* Android and browser today.
* iOS under development.

### What is supported

* Receives read-only MagicFolder invites via MagicWormhole and displays a listing of the files in their root directories ([#36](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/issues/36)).
* Decoding Tahoe-LAFS read-only directory capabilities ([#16](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/issues/16)).
* Manual input of capabilities (i.e. not via MagicWormhole) ([#17](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/issues/17)).
* On Android, clicking on a file causes it to be downloaded and launched as an intent.
* Only the newer [GBS](https://tahoe-lafs.readthedocs.io/en/latest/specifications/http-storage-node-protocol.html) protocol is supported, Foolscap might be implemented later.

* Coming soon:
  * Showing the full subdirectory hierarchy ([#39](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/issues/39)).
  * Importing read/write capabilities (which does not imply writing.) ([#46](https://gitlab.com/tahoe-lafs/tahoe-lafs-mobile/-/issues/46)).
  * Delete capability button.

* Coming later:
  * Writing via upload helper service, later without.
  * Android file system integration.

### Configuration

The list of storage nodes is configured by editing a yaml file within the app. It's planned to add more structure to this dialog to improve ergonomics and reduce the likelihood of errors.

The list of imported capabilities is persisted as a yaml file. There will soon be a delete button for each capability but for now the file would need lines deleted by hand to achieve this.

Backup apps should persist the above two files but not any cached content.

### Capability life-cycle

Folders (i.e. capabilities) have life-cycles that are independent of one another.
Upon app launch or adding a capability, the download of the contents list for each capability takes place while the UI shows a top-heavy hour glass icon.
Upon failure the icon changes to a cross, and on success it changes to a folder.
Upon a change of grid configuration the download starts anew but the already obtained contents are retained in case of failure or slow download.
This "redownloading" state is shown by a bottom-heavy hourglass icon and if the redownload fails then an exclamation mark icon is used.
In any of the states with data (folder, bottom-heavy or exclamation mark icons), clicking on the capability switches to the detailed listing of the contents.
A similar presentation of the downloading status of files is planned for the near future.

### Privacy policy

This program carries out network activity in order to fulfill requests from the user.
It does not contain any telemetry or tracking code,
and it does not transmit any information over the network except when necessary to do what the user asked, in a way that would not surprise a user that understands the tahoe-lafs protocol.
Any deviations from this statement should be immediately reported as a serious bug.

## Build Instructions

### Debug

Build an Android debug apk and install it to an Android device connected via USB
(with necessary permissions granted):

```sh
cd obelisk
nix-build -A android.frontend -o result-android-frontend
./result-android-frontend/bin/deploy
```

### Release

Build an Android release apk:

```sh
cd obelisk
nix-build -A android.frontend.release -o result-android-release
./result-android-release/bin/deploy
```

If you want to sign a build with the release key then you need to a signing key.

A new key store containing a signing key can be created with `keytool` like:

```sh
$ keytool \
    -genkey \
        -v \
        -keystore ./android-release.keystore \
        -alias release \
        -keyalg RSA \
        -keysize 2048 \
        -validity 36500 \
        -dname "CN=...;OU=...;O=...;L=...;ST=...;C=..."
```

The store password must be known to the release derivation.
`obelisk/default.nix` expects `obelisk/android-signing-key.nix` to exist to find this information.
See `obelisk/android-signing-key-example.nix` for an example of this file.
Note:

* values in the *dname* must have the following characters backslash-escaped: `,+=<>#;`.
* the validity period must be at least 50 years.
* the tool warns about using a proprietary key store format but this format seems to be required by reflex-platform.

Build an Android release Bundle (aab):

```sh
cd obelisk
nix-build -A android.frontend.bundle -o result-android-bundle
```

Ignore the ``deploy`` script in the Bundle result.
Bundles are not directly installable on devices.

### Run Locally

You can run the Obelisk application locally, without a phone.
To do so:

```sh
cd obelisk
nix-shell
ob run
```

This should eventually say something like ``Frontend running on http://localhost:8008`` and you can then visit that address.
Theoretically, "a Web browser" is what's needed but Obelisk shortcomings currently demand Chrome (or Chromium).
It is advisable to make the browser window "phone-shaped" for a decent experience.
You can use "Developer Tools" to exactly size it to particular phones if you prefer.


### Update Dependencies

#### nix-thunk

Some dependencies are managed using [nix-thunk](https://github.com/obsidiansystems/nix-thunk).
These can be found in ``obelisk/dep``.

Nix thunks are essentially references to git repositories.
These can be unpacked to their source in-place when working on the project or packed up into a few small files.

``nix-thunk`` is available in the nix shell provided by ``shell.nix`` at the top of the repository.

Consider an example dependency "botan".
From the top-level of this repo,
to clone a copy of the Botan git repository at the appropriate commit, run:

```sh
nix-thunk unpack obelisk/dep/botan
```

You can work on it in-place and when you are done, pack it up again:

```sh
nix-thunk pack obelisk/dep/botan
```

This requires that changes have been committed and pushed.

Note:
A bug in the current version of Obelisk (link?) causes trouble if certain repos are unpacked.
If you have any trouble running an ``ob`` command (ob run, ob repl, etc.) with a thunk unpacked,
try adding the flag ``--no-interpret obelisk/dep`` and hopefully that will sort it out.


#### adding a new one

Use ``nix-thunk create`` to add a new dependency.
For example, to add a thunk for *cowsay*:

```sh
nix-thunk create https://github.com/cowsay-org/cowsay obelisk/dep/cowsay
```

If the dependency is a Haskell library it needs to be added to:

* the Nix Haskell package set
  (see ``obelisk/frontend/frontend.cabal``)
* the relevant section of the project's cabal file
  (eg ``obelisk/frontend/frontend.cabal``)

#### updating an existing one

Use ``nix-thunk update`` to modify an existing dependency.
For example, to switch to the release-2023 branch of *cowsay*:

```sh
nix-thunk update --branch release-2023 obelisk/dep/cowsay
```

#### `ob run` interactions

``ob run`` does not pick up new dependencies.
Restart it *and* re-enter your ``nix-shell`` so it can pick up the changes.


#### the Android build

Test the Android build to make sure the new dependency works there too:

```sh
cd obelisk
nix-build -A android.frontend
```

{ pkgs, nix-thunk, fetchurl }:
self: super:
with pkgs.haskell.lib; {

  # Sometimes, I just want fast build times, and I don't care about tests,
  # documentation, or getting stuff from a cache.
  # From https://discourse.nixos.org/t/nix-haskell-development-2020/6170/2
  mkDerivation = args: super.mkDerivation (args // {
      doCheck = false;
      doHaddock = false;
      enableLibraryProfiling = false;
      enableExecutableProfiling = false;
      # jailbreak = true; # Maybe a bit too much hehe
      # Disable compiler optimizations for faster compilation:
      buildFlags = [ "--ghc-options=-O0" ];
    });

  # Use our fork of android-activity which adds support for VIEW intents.
  android-activity = import
    (nix-thunk.thunkSource ./dep/android-activity)
    { inherit (pkgs.buildPackages) jdk;
      inherit (self) callPackage callCabal2nix;
    };

  # AND OUR FORK OF REFLEX-DOM.
  reflex-dom = super.reflex-dom.overrideAttrs (old: {
    src = "${nix-thunk.thunkSource ./dep/reflex-dom}/reflex-dom";
  });

  # Here, we get the tahoe-chk and tahoe-ssk packages from their thunk and use
  # callCabal2nix to get nix derivations to build them.
  tahoe-chk = self.callCabal2nix
    "tahoe-chk"
    (nix-thunk.thunkSource ./dep/tahoe-chk)
    {};

  # magic-wormhole depends on this extra crypto library.
  saltine = super.saltine.overrideAttrs (old : {
    nativeBuildInputs = old.nativeBuildInputs ++ [ pkgs.pkg-config ];
    # saltine's Haddock phase isn't happy with our version of aeson
    doHaddock = false;
  });

  tahoe-ssk = self.callCabal2nix
    "tahoe-ssk"
    (nix-thunk.thunkSource ./dep/tahoe-ssk)
    {};

  tahoe-capabilities = self.callCabal2nix
    "tahoe-capability"
    (nix-thunk.thunkSource ./dep/tahoe-capabilities)
    {};

  tahoe-directory = self.callCabal2nix
    "tahoe-directory"
    (nix-thunk.thunkSource ./dep/tahoe-directory)
    {};

  # Similarly, get a package implementing Great Black Swamp.
  tahoe-great-black-swamp = self.callCabal2nix
    "tahoe-great-black-swamp"
    (nix-thunk.thunkSource ./dep/tahoe-great-black-swamp)
    {};

  # this is marked as broken, because there's no python to run the
  # integration tests; so we override to git our own pattern that
  # skips (just) the integration test
  spake2 = (self.callCabal2nix
    "spake2"
    (nix-thunk.thunkSource ./dep/haskell-spake2)
    {}).overrideDerivation (
      oldAttrs: {
        broken=false;
        dontCheck=true;
        checkPhase=''
set -x
runHook preCheck
./Setup test --test-options '--pattern "$2 != \"Integration\""'
runHook postCheck
'';
      }
    );

  # magic-wormhole base library (similar to above, we skip the
  # integration tests because no pyhthon)
  # Be explicit about the websockets version we want since
  # Obelisk takes them out for some reason.
  magic-wormhole = let
    websockets = self.callHackageDirect {
      pkg = "websockets";
      ver = "0.12.7.3";
      sha256 = "sha256-zQY5xQclPNZk7b14ut6Wzcgaolkx+brOxDO5FrZAzk8=";
    } {};
  in
  (self.callCabal2nix
    "haskell-magic-wormhole"
    (nix-thunk.thunkSource ./dep/haskell-magic-wormhole)
    { inherit websockets; }).overrideDerivation (
      oldAttrs: {
        broken=false;
        dontCheck=true;
        checkPhase=''
set -x
runHook preCheck
./Setup test --test-options '--pattern "$2 != \"Integration\""'
runHook postCheck
'';
      }
    );

  # And the integration library that pulls the pieces together and presents a
  # high-level interface.
  gbs-downloader = self.callCabal2nix
    "gbs-downloader"
    (nix-thunk.thunkSource ./dep/gbs-downloader)
    {};

  magic-foldr = self.callCabal2nix
    "magic-foldr"
    (nix-thunk.thunkSource ./dep/magic-foldr)
    {};

  # We also ended up needing an override of the base32 library, which we
  # obtain from Hackage.
  base32 = self.callHackageDirect {
    pkg = "base32";
    ver = "0.2.1.0";
    sha256 = "04glpmwp50qi29h8cb5j0w1rz0ww30nw4xgj2a3l7zh1gprhwj89";
  } {};

  # The test suite uses >35GB of RAM.
  servant-docs = dontCheck super.servant-docs;

  # A dependency of servant that's missing from reflex-platform.
  hspec-wai = self.callHackageDirect {
    pkg = "hspec-wai";
    ver = "0.11.1";
    sha256 = "sha256-0OSUQ6m6Bq+LGViNzW2WvPN+Nbd9bGjJsCZhOwb4oFo=";
  } {};

  # nixpkgs doesn't capture the Android dependencies for this package due to
  # conditionals in the cabal file or maybe CPP usage in the source.  Try to
  # fix it up.
  #
  # Possibly we should split tahoe-great-black-swamp into separate client and
  # server libraries so we don't need this dependency on Android at all.  This
  # is easier for now.
  simple-sendfile = self.callPackage ./dep/simple-sendfile.nix { };
  # Obelisk null's these on iOS - get them back:
  wai = self.callPackage ./dep/wai.nix { };
  warp = self.callPackage ./dep/warp.nix { };
  wai-app-static = self.callPackage ./dep/wai-app-static.nix { };

  # Some things don't like aeson 2.1 - aeson 2.1 is the best - make them shut up
  cborg-json = doJailbreak super.cborg-json;
  lens-aeson = doJailbreak super.lens-aeson;
  deriving-aeson = doJailbreak super.deriving-aeson;
  hie-bios = doJailbreak super.hie-bios;

  # servant-js test suite doesn't like our version of hspec.
  servant-js = dontCheck super.servant-js;

  # 0.19.1.0, the latest version, is marked as broken.  We still want it.
  language-ecmascript = unmarkBroken super.language-ecmascript;

  # Had a hard time trying to work around
  # https://github.com/reflex-frp/reflex-platform/blob/6fbaf9b5dafd3e1afc538049654fb8ab8ce64965/android/default.nix#L107-L110
  # until I found haskell zlib can also just use its own (c) zlib
  zlib = compose.enableCabalFlag "bundled-c-zlib" super.zlib;

  # cryptonite-0.30 contains a fix we require for iOS builds (prefixes for blake2b C symbols)
  # Thanks to jackdk of irc.libera.chat/reflex-frp for this tip!
  cryptonite = compose.disableCabalFlag "integer-gmp" (self.callHackageDirect {
    pkg = "cryptonite";
    ver = "0.30";
    sha256 = "sha256-x0N7Qff3gb9dNH8L026nki8snz1DpWIPerYBMCBFmBM=";
  } {});
}

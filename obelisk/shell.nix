# Use one of Obelisk/Reflex's shells.  This at least gets us a working ghcid
# and haskell-language-server, extremely useful for development purposes.
(import ./default.nix { }).shells.ghc.overrideAttrs (old: {
  shellHook =
    ''
    alias tlm-lint="hlint -j ."
    alias tlm-style="find . -type f -name '*.hs' -print0 | xargs -0 stylish-haskell --inplace"
    alias tlm-all="tlm-lint && tlm-style"

    # Print overview when entering interactive shell
    if [[ $- == *i* ]]
    then
      echo "Some aliases for your convenience:"
      # 'tlm' stands for 'Tahoe Lafs Mobile'
      alias -p | fgrep 'tlm-' | cut -f"2-" -d" "
    fi
    '';
})

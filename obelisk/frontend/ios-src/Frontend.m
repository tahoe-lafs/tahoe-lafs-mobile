#include "HsFFI.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

const char * applicationDocumentsPathC() {
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    if(!url) return NULL;
    NSString *path = [url path];
    if(!path) return NULL;
    return [path UTF8String];
}

const char * applicationCachesPathC() {
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    if(!url) return NULL;
    NSString *path = [url path];
    if(!path) return NULL;
    return [path UTF8String];
}

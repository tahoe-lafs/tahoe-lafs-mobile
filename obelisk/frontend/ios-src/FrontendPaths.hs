-- | Implement platform-specific path-related operations for the Linux
-- platform.
{-# LANGUAGE ForeignFunctionInterface #-}
module FrontendPaths (getFilesDir, getCacheDir, runWithRef, viewFile) where

import           Data.ByteString                       (packCString)
import qualified Data.ByteString                       as BS
import           Data.Default.Class                    (def)
import           Data.IORef                            (IORef, newIORef)
import qualified Data.Text                             as Text
import           Data.Text.Encoding                    (decodeUtf8)
import           Foreign.C.String                      (CString)
import           Foreign.C.Types                       (CBool (..), CInt (..))
import           Foreign.Ptr                           (Ptr, nullPtr)
import           Foreign.StablePtr                     (StablePtr,
                                                        deRefStablePtr,
                                                        newStablePtr)
import           Language.Javascript.JSaddle           (JSM)
import           Language.Javascript.JSaddle.WKWebView (mainBundleResourcePath)
import           PlatformEvents                        (PlatformEventCallbacks)
import           Reflex.Dom                            (run)
import           System.Directory                      (createDirectoryIfMissing)
import           System.FilePath                       ((</>))


-- | Get a dedicated directory for this application beneath the XDG standard
-- cache directory.
getCacheDir :: IO (Maybe FilePath)
getCacheDir = do
  Just cachesPath <- applicationCachesPath
  let dataPath = Text.unpack (decodeUtf8 cachesPath) </> "tahoelafsmobile"

  createDirectoryIfMissing True dataPath
  pure $ Just dataPath

-- | Get the path to a directory that is specific to the application where
-- internal application data may be written (and read back).
getFilesDir :: IO (Maybe FilePath)
getFilesDir = do
  Just documentsPath <- applicationDocumentsPath
  let dataPath = Text.unpack (decodeUtf8 documentsPath) </> "tahoelafsmobile"

  createDirectoryIfMissing True dataPath
  pure $ Just dataPath

-- | Run a frontend on iOS (not `ob run`).
runWithRef :: (Maybe (IORef PlatformEventCallbacks) -> JSM ()) -> IO ()
runWithRef makeJSM =
  -- Off Android, we have no platform integration or event handlers so just
  -- supply a no-op callback to fill the slot and then proceed.
  newIORef def >>= run . makeJSM . Just

-- | Show a file to the user.
viewFile :: FilePath -> IO ()
viewFile p = print $ "I viewed " <> p


foreign import ccall applicationDocumentsPathC :: IO CString
foreign import ccall applicationCachesPathC :: IO CString

applicationDocumentsPath :: IO (Maybe BS.ByteString)
applicationDocumentsPath = do
    bs <- applicationDocumentsPathC
    if bs == nullPtr
        then return Nothing
        else Just <$> packCString bs

applicationCachesPath :: IO (Maybe BS.ByteString)
applicationCachesPath = do
    bs <- applicationCachesPathC
    if bs == nullPtr
        then return Nothing
        else Just <$> packCString bs

{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module WormholeInvite (InviteMessage(..), receiveInvite, doWormholeInviteOrCapAdd) where

import           Control.Exception               (Exception, SomeException,
                                                  throwIO, try)
import           Control.Monad.IO.Class          (liftIO)
import           Crypto.Spake2                   (makePassword)
import qualified Data.Aeson                      as A
import           Data.Aeson.Types                (FromJSON (..), ToJSON (..),
                                                  object, parseJSON, toJSON,
                                                  (.:), (.=))
import qualified Data.Aeson.Types                as AesonTypes
import qualified Data.ByteString                 as B
import qualified Data.ByteString.Lazy            as LBS
import           Data.Text                       (breakOn, tail)
import qualified Data.Text                       as T
import qualified Data.Text.Encoding              as T
import           GHC.Conc                        (atomically)
import           MagicWormhole
import           MagicWormhole.Internal.Messages (Mood (..))

import           Structs

-- | Parsing the magic-folder invitation failed.
newtype InviteParseError = InviteParseError B.ByteString deriving stock (Show, Eq)
instance Exception InviteParseError

-- | Represent the version of a magic-folder invitation.
data InvitesVersion0 = InvitesVersion0 deriving stock (Eq, Show)


instance FromJSON InviteMessage where
  parseJSON (AesonTypes.Object v) =
    InviteMessage <$> v .: "kind" <*> v .: "folder-name" <*> v .: "collective"
  parseJSON _ = error "fixme"


-- | Complete a Magic-Folder invitation conversation as a receiver over the
-- given wormhole connection.
performInvite :: EncryptedConnection -> IO InviteMessage
performInvite conn = do
  MagicWormhole.PlainText invite_msg <- atomically $ receiveMessage conn
  -- todo: should check we got a "read-only" invite
  print invite_msg
  sendMessage conn (MagicWormhole.PlainText "{\"protocol\": \"invite-v1\", \"kind\": \"join-folder-accept\"}")
  MagicWormhole.PlainText ack_msg <- atomically $ receiveMessage conn
  print ack_msg
  -- pure $ fromMaybe (InviteMessage "invite-v1" "foo" "dummy") (A.decode (LBS.fromStrict invite_msg))
  -- not ^ because we want an error
  case A.decode (LBS.fromStrict invite_msg) of
    Nothing -> throwIO $ InviteParseError invite_msg
    Just x  -> pure x


instance ToJSON InvitesVersion0 where
  toJSON InvitesVersion0 =
    object ["magic-folder" .= object ["supported-messages" .= ["invite-v1" :: String]]]

instance FromJSON InvitesVersion0 where
  parseJSON (AesonTypes.Object v) = do
    (AesonTypes.Object _mf) <- v .: "magic-folder"
--    supported <- mf .: "supported-messages"
    return InvitesVersion0
  parseJSON _ = error "fixme"


receiveInvite :: T.Text -> T.Text -> MagicWormhole.Session -> IO InviteMessage
receiveInvite nameplate code session = do
  mailbox <- liftIO $ MagicWormhole.claim session (MagicWormhole.Nameplate nameplate)
  peer <- liftIO $ MagicWormhole.open session mailbox
  let passcode = T.encodeUtf8 . T.append (T.append nameplate "-") $ code
  result <- liftIO $ MagicWormhole.withEncryptedConnection peer (Crypto.Spake2.makePassword passcode) InvitesVersion0 performInvite
  MagicWormhole.release session (Just (MagicWormhole.Nameplate nameplate))
  _x <- MagicWormhole.close session (Just mailbox) (Just Happy)
  pure result

-- | Take a code like "1-foo-bar" and split off the mailbox, returning a tuple
-- like ("1", "foo-bar")
splitWormholeCode :: T.Text -> (T.Text, T.Text)
splitWormholeCode entireCode = do
  let (n, c) = breakOn (T.pack "-") entireCode
  let code = Data.Text.tail c
  (n, code)

checkIfCapability :: T.Text -> Maybe (T.Text, T.Text)
checkIfCapability input =
    --  Try "name: URI:DIR2-RO:cap" format
    case T.splitOn ": URI:DIR2-RO:" input of
        [name, cap] -> Just (name, "URI:DIR2-RO:" <> cap)
        -- Try cap format without name "URI:DIR2-RO:cap"
        _ -> case T.stripPrefix "URI:DIR2-RO:" input of
            Just cap ->
                -- Generate a name by hashing the cap instead of showing part of it
                Just (generateCapName cap, "URI:DIR2-RO:" <> cap)
            Nothing -> Nothing
  where
    generateCapName :: T.Text -> T.Text
    generateCapName cap =
        "Tahoe-" <> (T.pack . take 8 . show . hash . T.unpack $ cap)
      where
        hash :: String -> Int
        hash = foldr (\c s -> s * 33 + fromEnum c) 5381

-- | public API
doWormholeInviteOrCapAdd :: T.Text -> IO (Either SomeException InviteMessage)
doWormholeInviteOrCapAdd input =
    -- first we check if its capability
    case checkIfCapability input of
        Just (name, cap) ->
            -- For a capability, we just create an inviteMessage here
            pure $ Right $
                      InviteMessage
                          { kind = "invite-v1"
                          , folderName = name
                          , collectiveReadCap = cap
                          }
        -- if not a capability then we just do the wormhole invite
        Nothing -> try $ do
            side <- MagicWormhole.generateSide
            MagicWormhole.runClient endpoint appID side Nothing (receiveInvite nameplate onlycode)
          where
            -- codes come in like: "1-foo-bar"
            (nameplate, onlycode) = splitWormholeCode input
            -- "ws://relay.magic-wormhole.io:4000/v1"
            endpoint = MagicWormhole.WebSocketEndpoint "relay.magic-wormhole.io" 4000 "/v1"
            appID = MagicWormhole.AppID "private.storage/magic-folder/invites"


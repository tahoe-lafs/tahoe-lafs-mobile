{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecursiveDo         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

-- This is doubling as a place for stuff that's needed in multiple unrelated places

module Structs (module Structs, Announcements) where

import           Control.Exception
import           Data.Map.Strict       (Map)
import           Data.Text             (Text)
import qualified Data.Text             as T
import           Data.Void             (Void)
import           Tahoe.Announcement    (Announcements)
import qualified Tahoe.Directory       as Directory
import           Tahoe.MagicFoldr      (FolderEntry (..))
import qualified Tahoe.SDMF            as SDMF
import           Text.Megaparsec.Error (ParseErrorBundle)


type ReadCap = Directory.DirectoryCapability SDMF.Reader

data Error = Dunno
           | Debug Text
           | ReadFileThrew Text
           | CantFindConfigDir
           | CantParseFoldersFileAsMap Text
           | CantParseInvite (ParseErrorBundle Text Void)
           | GetFolderFilesSaid Text
           | StillDownloading
           | NoWormhole
           deriving stock (Eq)
           deriving anyclass Exception

instance Show Error where
  show Dunno = "Unknown error"
  show (Debug t) = "Debug: " <> T.unpack t
  show (ReadFileThrew t) = "IO error when reading file: " <> T.unpack t
  show CantFindConfigDir = "Failed to ascertain config directory"
  show (CantParseFoldersFileAsMap t) = "Folders file unparsable: " <> T.unpack t
  show (CantParseInvite e) = "The invite was unparsable: " <> show e
  show (GetFolderFilesSaid t) = "getFolderFiles failed with: " <> T.unpack t
  show StillDownloading = "This folder's contents listing is still downloading"
  show NoWormhole = "The wormhole code was not recognised"

type FolderName = Text
data FolderPhase = Downloadable
                 | Downloading (Maybe FolderEntry)
                 | Downloaded (Either Error FolderEntry)
                 | Disappeared FolderEntry deriving stock (Show, Eq)
type Folders = Map FolderName (ReadCap, FolderPhase)

-- This must always be "invite-v1".
data InviteMessage = InviteMessage
    { kind              :: Text
    , folderName        :: Text
    , collectiveReadCap :: Text
    } deriving stock (Eq, Show)


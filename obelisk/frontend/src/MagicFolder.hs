{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -Wno-missing-deriving-strategies #-}

module MagicFolder where

import           Control.Exception.Base (Exception, SomeException, try)
import           Control.Monad          (forM, when)
import           Control.Monad.IO.Class (MonadIO, liftIO)
import           Data.Bifunctor         (first)
import qualified Data.ByteString        as B
import           Data.List              (find)
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as Map
import qualified Data.Text              as T
import qualified Data.Text.Encoding     as TE (decodeUtf8, encodeUtf8)
import           Data.Yaml              (decodeEither', encode)
import           Data.Yaml.Aeson        (ParseException)
import           FrontendPaths          (getFilesDir)
import           Obelisk.Configs        (HasConfigs (getConfig))
import           Structs
import           System.Directory.Extra (createDirectoryIfMissing,
                                         doesFileExist, removeFile)
import           System.FilePath        ((</>))
import           Tahoe.Announcement     (Announcements (..))
import qualified Tahoe.Directory        as Directory
import           Tahoe.Download
import qualified Tahoe.MagicFoldr       as MF
import           Text.Megaparsec        (parse)

data NamedFolder = NamedFolder
    { folderName  :: T.Text
    , folderState :: MF.MagicFolder
    }
    deriving stock (Eq, Ord, Show)

-- | The name of some Magic-Folder entry (file, directory, etc) in the context
-- of its container (parent directory).
newtype EntryName = EntryName {unEntryName :: T.Text}
    deriving stock (Eq, Ord, Show)

data UpdatingGridConfig
    = NotUpdating
    | UpdatingGrid
    deriving (Eq, Ord, Show)

-- | Represent the UI state related to handling folder invites.
data AcceptingInviteState
    = -- | The UI is not currently waiting for an invite code to be entered.
      NotAccepting
    | -- | The UI *is* currently waiting for an invite code to be entered.
      EnteringCode
    | -- | We've entered a code and await the conclusion of the invite.
      ReceivingInvite
    | -- | We tried to process an invite code but encountered some problem.
      ReceivingInviteFailed T.Text
    | -- | The invite has concluded successfully.
      InviteFinished
    deriving stock (Eq, Ord, Show)

-- | Checks if the given text is valid and parsable YAML
isValidYaml :: T.Text -> Bool
isValidYaml textContent =
    (textContent == "")
        || ( case decodeEither' (TE.encodeUtf8 textContent) ::
                    Either ParseException Tahoe.Announcement.Announcements of
                Left _  -> False
                Right _ -> True
           )

-- | Deletes the custom server configuration file.
deleteGridConfig :: IO ()
deleteGridConfig = do
    dir <- getFilesDir
    case dir of
        Just dr -> do
            let configDir = dr </> "custom-config"
            let path = configDir </> "servers.yaml"
            exists <- System.Directory.Extra.doesFileExist path
            when exists $ System.Directory.Extra.removeFile path
        Nothing -> putStrLn "Error: Could not determine files directory."

-- | Saves the server configuration to a YAML file.
saveGridConfig :: Tahoe.Announcement.Announcements -> IO ()
saveGridConfig announcements = do
    dir <- getFilesDir
    case dir of
        Just dr -> do
            let configDir = dr </> "custom-config"
            System.Directory.Extra.createDirectoryIfMissing True configDir
            let path = configDir </> "servers.yaml"
            B.writeFile path (encode announcements)
        Nothing -> putStrLn "Error: Could not determine files directory."

-- | Create an invite state for a given exception.
-- Just shows the exception and wraps it in the error state value.
stateForInviteFailure ::
    (Control.Exception.Base.Exception e) => e -> AcceptingInviteState
stateForInviteFailure e = ReceivingInviteFailed (T.pack . show $ e)

-- | Extract the text of an error from the current invite state, if there is
-- any.
errorText :: AcceptingInviteState -> Maybe T.Text
errorText (ReceivingInviteFailed t) = Just t
errorText _                         = Nothing

-- | Loads the grid announcements for downloads.
--
-- This function reads the grid configuration from either a custom "servers.yaml" file
-- located in the application's private files directory under "custom-config/servers.yaml",
-- or from the default "common/servers.yaml" if no custom configuration is found.
--
-- The configuration is expected to be in YAML format. It is parsed using the 'aeson' library,
-- relying on the 'FromJSON' and 'ToJSON' instances defined for the 'Announcements' type in
-- gbd-downloader.
loadGrid :: (HasConfigs m, MonadIO m) => m (Either T.Text Announcements)
loadGrid = do
  mCustomConfig <- liftIO getCustomConfig
  case mCustomConfig of
    Just config -> pure $ first renderError $ decodeEither' config
    Nothing -> do
      announcementsBytes <- getConfig "common/servers.yaml"
      case announcementsBytes of
        Nothing    -> pure . Left $ "Could not load grid configuration"
        Just bytes -> pure $ first renderError $ decodeEither' bytes
  where
    renderError = T.concat . ("Could not parse grid configuration: " :) . (: []) . T.pack . show

getCustomConfig :: IO (Maybe B.ByteString)
getCustomConfig = do
  dir <- getFilesDir
  case dir of
    Just dr -> do
      let path = dr </> "custom-config" </> "servers.yaml"
      exists <- System.Directory.Extra.doesFileExist path
      if exists
        then Just <$> B.readFile path
        else pure Nothing
    Nothing -> pure Nothing

-- | Load the configured magic-folder collective capabilities.
loadFolderMap :: (MonadIO m) => m (Either Error (Map T.Text T.Text))
loadFolderMap = do
    appdir <- liftIO getFilesDir
    case appdir of
        Just fd -> do
            let fname = fd <> "/magicfolders.yaml"
            collectivesBytes <- liftIO $ try $ B.readFile fname
            case collectivesBytes of
                -- todo: too broad
                Left (_ :: SomeException) -> pure $ Right mempty
                Right bytes ->
                    case decodeEither' bytes of
                        Left err -> pure $ Left $ CantParseFoldersFileAsMap $ T.pack $ show err
                        Right (mp :: Map T.Text T.Text) -> pure $ Right mp
        Nothing -> pure $ Left CantFindConfigDir

-- | First downloads a directory using its capability, then attempts to process it.
-- |
-- | If no @metadata is found, processes as a plain Tahoe directory, recursively building a tree
-- | where each FolderEntry is either a file or directory (containing more FolderEntries).
-- |
-- | For plain Tahoe directories, returns errors for failed directory downloads or parsing.
-- | If @metadata exists, processes as a magic folder using the MF library, which can also include conflicts.
-- |
-- | Returns Left with error if:
-- | - Initial directory download fails
-- | - Directory parsing fails (invalid capability)
-- | - Magic folder processing fails
-- | Otherwise returns Right with a FolderEntry representing the directory tree
readFolder ::
  (MonadIO m) => Announcements -> NamedFolder -> m (Either Error MF.FolderEntry)
readFolder (Announcements ann) NamedFolder {folderState = state} = do
  let capability = MF.folderCollectiveReadCap state
  -- First attempt to download the directory from the Tahoe grid
  dirParseAttempt <-
    downloadDirectory ann capability announcementToMutableStorageServer
  case dirParseAttempt of
    Right dir -> do
      case findMetadata dir of
        Nothing -> do
          -- Process as plain Tahoe directory: recursively build directory tree of files and directories
          let processDirectory :: Directory.Directory -> IO (Either Error (Map T.Text MF.FolderEntry))
              processDirectory currentDir = do
                entryResults <- forM (Directory.directoryChildren currentDir) $ \entry -> do
                  let rawCap = Directory.entryReader entry
                  -- Try to parse as SDMF capability (another directory node)
                  case parse Directory.pReadSDMF "sdmf-cap" (TE.decodeUtf8 rawCap) of
                    Right sdmfCap -> do
                      -- Download and process nested directory recursively
                      nestedResult <- downloadDirectory ann sdmfCap announcementToMutableStorageServer
                      case nestedResult of
                        Right nestedDir -> do
                          nestedContents <- processDirectory nestedDir
                          case nestedContents of
                            Right contents -> pure $ Right (Directory.entryName entry, MF.Directory contents)
                            Left err -> pure $ Left err
                        Left err ->
                          pure $ Left $ GetFolderFilesSaid $ "Invalid directory capability or network error: " <> T.pack (show err)
                    Left _ ->
                      -- If not directory, try to parse as file
                      case parse Directory.pReadCHK "chk-cap" (TE.decodeUtf8 rawCap) of
                        Right contentCap -> do
                          let contentReader = case contentCap of
                                Directory.DirectoryCapability r -> r
                          pure $ Right (Directory.entryName entry, MF.File contentReader contentReader)
                        Left err ->
                          pure $ Left $ GetFolderFilesSaid $ "Invalid file capability: " <> T.pack (show err)
                case sequence entryResults of
                  Right results -> pure $ Right $ Map.fromList results
                  Left err      -> pure $ Left err

          contents <- liftIO $ processDirectory dir
          case contents of
            Right c  -> pure $ Right $ MF.Directory c
            Left err -> pure $ Left err
        Just _ -> do
          -- Magic folder detected (@metadata present), use MF library
          -- This can return tree of directories, files, and/or conflicts
          result <- liftIO $ MF.getFolderFiles ann state
          case result of
            Right d  -> pure $ Right d
            Left err -> pure $ Left $ GetFolderFilesSaid $ T.pack $ show err
    Left err -> pure $ Left $ GetFolderFilesSaid $ "Failed to access directory: " <> T.pack (show err)
  where
    -- Helper to detect magic folder by checking for @metadata marker
    findMetadata dir =
      find
        (\e -> Directory.entryName e == "@metadata")
        (Directory.directoryChildren dir)

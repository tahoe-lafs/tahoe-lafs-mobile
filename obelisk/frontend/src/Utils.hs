
module Utils (module Utils) where

-- import Debug.Trace
-- traceme :: Show a => String -> a -> a
-- traceme s a = trace (s <> show a) a

-- These are to avoid writing things like
--   fmap func <$> eventContainingAList
-- which there'd be a lot of in this project.
-- You say
--   func <$$> eventContainingAList
-- instead

infixr 8 <$$>
(<$$>) :: (Functor f0, Functor f1) =>
          (a -> b)
       -> f1 (f0 a)
       -> f1 (f0 b)
(<$$>) = fmap . fmap

infixr 8 <$$$>
(<$$$>) :: (Functor f0, Functor f1, Functor f2) =>
          (a -> b)
       -> f2 (f1 (f0 a))
       -> f2 (f1 (f0 b))
(<$$$>) = fmap . fmap . fmap

infixl 1 <&&>
(<&&>) :: (Functor f0, Functor f1) =>
          f1 (f0 a)
       -> (a -> b)
       -> f1 (f0 b)
(<&&>) = flip (fmap . fmap)

infixl 1 <&&&>
(<&&&>) :: (Functor f0, Functor f1, Functor f2) =>
          f2 (f1 (f0 a))
       -> (a -> b)
       -> f2 (f1 (f0 b))
(<&&&>) = flip (fmap . fmap . fmap)


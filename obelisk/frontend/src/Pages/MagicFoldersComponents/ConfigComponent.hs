{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecursiveDo         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Pages.MagicFoldersComponents.ConfigComponent where

import           Control.Monad          (void)
import           Control.Monad.Fix
import           Control.Monad.IO.Class
import qualified Data.Text              as T
import qualified Data.Text.Encoding     as TE
import qualified Data.Yaml              as Y
import           MagicFolder
import           Pages.Widgets
import           Reflex
import           Reflex.Dom
import qualified Tahoe.Announcement



-- | Creates a dialog for editing the server configuration.
--
-- This function creates a Material Design Components (MDC) dialog that allows
-- the user to view and edit the server configuration in a text area. The dialog
-- is shown or hidden based on the 'visible' dynamic. The current configuration
-- is displayed in the text area, and changes are applied when the user clicks
-- "OK" or presses Enter. Clicking "Cancel" or closing the dialog discards
-- changes. If the text area is empty when "OK" is clicked, the custom server
-- configuration is deleted.
--
-- The 'config' dynamic provides the current server configuration. The
-- 'updateConfigEvent' is an IO action that is called when the user accepts
-- changes, allowing the application to update its internal state.

gridConfigDialog ::
    ( PerformEvent t m
    , DomBuilder t m
    , PostBuild t m
    , MonadSample t m
    , MonadSample t (Performable m)
    , MonadIO (Performable m)
    , MonadFix m
    ) =>
    Dynamic t Bool ->
    Dynamic t (Either T.Text Tahoe.Announcement.Announcements) ->
    (() -> IO ()) ->
    m (Event t (Either () T.Text))
gridConfigDialog visible config updateConfigEvent = do
    rec (_, textAreaEl, (closeButton, acceptButton), exitButton) <- mdcDialog
            visible
            True
            (text "Grid Config")
            ( divClass "text-field-row text-field-row-fullwidth" $
                divClass "text-field-container" $ do
                    let baseClass = "mdc-text-field text-field mdc-text-field--fullwidth mdc-text-field--textarea mdc-text-field--outlined"
                    let dynAttrs =
                            ffor yamlValidDyn $
                                \isValidYAML ->
                                    if isValidYAML
                                        then "class" =: baseClass
                                        else "class" =: (baseClass <> " mdc-text-field--invalid")
                    result <- elDynAttr "div" dynAttrs $ do
                        let yamlDyn = fmap (either id encodeYaml) config
                        initialValue <- sample (current yamlDyn)
                        inputEl <-
                            textAreaElement
                                textAreaElementConfig'
                                    { _textAreaElementConfig_initialValue = initialValue
                                    , _textAreaElementConfig_setValue = Just $ updated yamlDyn
                                    }

                        -- Notched outline structure
                        elClass "div" "mdc-notched-outline" $ do
                            elClass "div" "mdc-notched-outline__leading" blank
                            elClass "div" "mdc-notched-outline__notch" $
                                elClass "label" "mdc-floating-label" blank
                            elClass "div" "mdc-notched-outline__trailing" blank

                        pure inputEl

                    elClass "div" "mdc-text-field-helper-line"
                        $ elDynAttr
                            "p"
                            ( ffor yamlValidDyn $ \isValid ->
                                if isValid
                                    then "class" =: "mdc-text-field-helper-text mdc-text-field-helper-text--persistent"
                                    else "class" =: "mdc-text-field-helper-text mdc-text-field-helper-text--persistent mdc-text-field-helper-text--validation-msg"
                            )
                        $ dynText
                        $ ffor yamlValidDyn
                        $ \isValid ->
                            if isValid
                                then "Add config or leave blank for test grid."
                                else "Invalid YAML."

                    pure result
            )
            $ do
                (lCloseButton, _) <- mdcButton "close" $ do
                    divClass "mdc-button__ripple" blank
                    divClass "mdc-button__label" $ text "Cancel"
                (lAcceptButton, _) <- mdcButtonDynEnabled yamlValidDyn "Save" $ do
                    divClass "mdc-button__ripple" blank
                    divClass "mdc-button__label" $ text "Save"
                pure (lCloseButton, lAcceptButton)

        let yamlValidDyn = isValidYaml <$> _textAreaElement_value textAreaEl
            goAheadEv = leftmost [keypress Enter textAreaEl, void (domEvent Click acceptButton)]

        updatedConfigEv <- performEvent $ ffor goAheadEv $ \() -> do
            textAreaContent <- sample $ current $ _textAreaElement_value textAreaEl
            if T.null textAreaContent
                then do
                    liftIO deleteGridConfig
                    liftIO $ updateConfigEvent ()
                    pure $ Right textAreaContent
                else do
                    let result = Y.decodeEither' (TE.encodeUtf8 textAreaContent) :: Either Y.ParseException Tahoe.Announcement.Announcements
                    case result of
                        Left err -> do
                            liftIO $ putStrLn $ "Error parsing YAML: " ++ show err
                            pure $ Left ()
                        Right announcements -> do
                            liftIO $ saveGridConfig announcements
                            liftIO $ updateConfigEvent ()
                            pure $ Right textAreaContent
    let textFinishedEv = leftmost [Left () <$ domEvent Click exitButton, Left () <$ domEvent Click acceptButton, Left <$> domEvent Click closeButton, updatedConfigEv]
    pure textFinishedEv
  where
    textAreaElementConfig = def
    textAreaElementConfig' =
        textAreaElementConfig
            { _textAreaElementConfig_elementConfig =
                (_textAreaElementConfig_elementConfig textAreaElementConfig)
                    { _elementConfig_initialAttributes =
                        "class" =: "mdc-text-field__input"
                            <> "rows" =: "16"
                            <> "cols" =: "60"
                    }
            }
    encodeYaml :: Tahoe.Announcement.Announcements -> T.Text
    encodeYaml = TE.decodeUtf8 . Y.encode

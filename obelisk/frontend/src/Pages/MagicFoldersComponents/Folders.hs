{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecursiveDo         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Pages.MagicFoldersComponents.Folders where

import           Common.Route
import           Control.Monad          (void)
import           Control.Monad.Fix      (MonadFix)
import           Data.Functor           (($>))
import qualified Data.Map               as Map
import           Data.Text              (Text)
import qualified Data.Text              as T
import           MagicFolder
import           Obelisk.Route.Frontend
import           Pages.Widgets          (mdcButton, mdcDialog, mdcText,
                                         showErrorBox)
import           Reflex.Dom.Core
import           Structs


phaseIconName :: FolderPhase -> Text
phaseIconName  Downloadable          = "description"    -- This looks like a file to me;
phaseIconName (Downloading Nothing)  = "hourglass_top"  --   it's not used yet anyway so we can revise it later.
phaseIconName (Downloading (Just _)) = "hourglass_bottom"
phaseIconName (Downloaded (Right _)) = "folder"
phaseIconName (Downloaded (Left _))  = "close"
phaseIconName (Disappeared  _)       = "error"

-- | A widget representing a single magic-folder.

folderWidget :: ( DomBuilder t m
                , SetRoute t (R FrontendRoute) m
                , MonadFix m
                , PostBuild t m
                , MonadHold t m
                ) => (FolderName, (ReadCap, FolderPhase)) -> m ()
folderWidget (name, (_cap, phase)) = do
    (theEl', _) <- elClass' "li" "mdc-deprecated-list-item" $ do
      elClass "span" "mdc-deprecated-list-item__ripple" blank
      elAttr "span" ("class" =: "mdc-deprecated-list-item__graphic material-icons") $
        text $ phaseIconName phase
      elAttr "span" ("class" =: "mdc-deprecated-list-item__text") $ do
        text name
      -- Delete button.  TODO: Wiring.
      -- elAttr "span" ("class" =: "mdc-deprecated-list-item__meta") $
        -- elAttr "button" ("class" =: "mdc-icon-button") $ do
        --   divClass "mdc-icon-button__ripple" blank
        --   elAttr "i" ("class" =: "material-icons") $ do
        --     text "delete"

    elClass "li" "mdc-deprecated-list-divider mdc-deprecated-list-divider--inset" blank

    case phase of
      Downloaded (Right _) -> setRoute    $ domEvent Click theEl' $>  FrontendRoute_FileBrowser :/ (name, [])
      Disappeared _        -> setRoute    $ domEvent Click theEl' $>  FrontendRoute_FileBrowser :/ (name, [])
      Downloading (Just _) -> setRoute    $ domEvent Click theEl' $>  FrontendRoute_FileBrowser :/ (name, [])
      Downloading Nothing  -> showErrorBox $ domEvent Click theEl' $>  StillDownloading
      Downloaded (Left e)  -> showErrorBox $ domEvent Click theEl' $>  e
      _ -> pure ()


-- | A widget where a wormhole code can be submitted and when it is, the
-- currently active route is changed to the invite-handling route.
receiveFolderInviteText
    :: ( PerformEvent t m
       , DomBuilder t m
       , PostBuild t m
       )
    => Dynamic t Bool -> m (Event t (Either () T.Text))
receiveFolderInviteText visible = do
    -- A modal dialogue asking for a wormhole code.
      (_, inputEl, (closeButton, acceptButton), _) <- mdcDialog visible False
        (text "Add Directory") -- Modal title
        (mdcText $ do -- Content of the modal:
          elClass "span" "mdc-text-field__ripple" blank
          inputEl <- inputElement $ def & inputElementConfig_elementConfig . elementConfig_initialAttributes .~
                    ("class" =: "mdc-text-field__input" <> "type" =: "text" <> "placeholder" =: "Capability or Invite Code")
          elClass "span" "mdc-floating-label" $ text "Enter Cap or Wormhole Invite"
          elClass "span" "mdc-line-ripple" blank
          pure inputEl
        ) $ do -- Action buttons at the bottom of the modal:
        (closeButton, _) <- mdcButton "close" $ do
          divClass "mdc-button__ripple" blank
          divClass "mdc-button__label" $ text "Cancel"
        (acceptButton, _) <- mdcButton "accept" $ do
          divClass "mdc-button__ripple" blank
          divClass "mdc-button__label" $ text "OK"
        pure (closeButton, acceptButton)

      let goAheadEv = leftmost [keypress Enter inputEl, void (domEvent Click acceptButton)]
          inviteCodeEv = tag (current (_inputElement_value inputEl)) goAheadEv
          inviteFinishedEv = leftmost [Left <$> domEvent Click closeButton, Right <$> inviteCodeEv]

      pure inviteFinishedEv

-- | Compute element attributes for the top-level element of an MDC
-- indeterminate linear progress indicator, given two values which decide its
-- visibility by their equality.
inviteProgressAttributes :: Eq a => a -> a -> Map.Map T.Text T.Text
inviteProgressAttributes when now'
  | when == now' = "class" =: commonClass <> commonParts
  | otherwise    = "class" =: (commonClass <> " mdc-linear-progress--closed") <> commonParts
    where
      commonClass = "mdc-linear-progress mdc-linear-progress--indeterminate"
      commonParts =
          "role" =: "progressbar"
          <> "aria-label" =: "Invite Progress"
          <> "aria-valuemin" =: "0"
          <> "aria-valuemax" =: "1"
          <> "aria-valuenow" =: "0"
          <> "data-mdc-auto-init" =: "MDCLinearProgress"

-- | Show something for invite progress
indeterminateLinearProgress
    :: (DomBuilder t m, PostBuild t m) => Dynamic t AcceptingInviteState -> m ()
indeterminateLinearProgress acceptInviteState = do
  elDynAttr "div" progressClasses $ do
    elAttr "div" ("class" =: "mdc-linear-progress__buffer") $ do
      elAttr "div" ("class" =: "mdc-linear-progress__buffer-bar") blank
      elAttr "div" ("class" =: "mdc-linear-progress__buffer-dots") blank
    elAttr "div" ("class" =: "mdc-linear-progress__bar mdc-linear-progress__primary-bar") $ do
      elAttr "span" ("class" =: "mdc-linear-progress__bar-inner") blank
    elAttr "div" ("class" =: "mdc-linear-progress__bar mdc-linear-progress__secondary-bar") $ do
      elAttr "span" ("class" =: "mdc-linear-progress__bar-inner") blank
  where
    -- If you want the progress bar to always be visible, try something like this:
    -- progressClasses = inviteProgressAttributes 1 <$> constDyn 1
    progressClasses = inviteProgressAttributes ReceivingInvite <$> acceptInviteState


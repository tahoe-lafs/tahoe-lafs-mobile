{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecursiveDo         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Pages.Widgets (showErrorBox, errorBox, mdcButtonDynEnabled, mdcButton, mdcDialog, mdcText, mdcTextArea) where

import qualified Data.Map.Strict   as Map
import           Data.Maybe        (isJust)
--import           Data.Text              (Text)
import           Control.Monad.Fix (MonadFix)
import qualified Data.Text         as T
import           Reflex.Dom.Core
import           Structs


-- | Show Just an error message on the page or nothing when the error message
-- is Nothing.  Also return an Event that triggers when the error should be
-- dismissed.
errorBox
    :: ( PostBuild t m
       , DomBuilder t m
       )
    => Dynamic t (Maybe T.Text)
    -> m (Event t ())
errorBox msg = do
  (_, _, closeButton, _) <- mdcDialog visible False (text "Error") (dynText msg') $ do
    (closeButton, _) <- mdcButton "close" $ do
      divClass "mdc-button__ripple" blank
      divClass "mdc-button__label" $ text "Dismiss"
    pure closeButton
  pure $ domEvent Click closeButton
  where
    visible = isJust <$> msg
    msg' = ffor msg $ \case
      Nothing  -> ""
      Just txt -> txt

-- | Simplify the use of errorBox and herd any magic strings into
-- the Show instance of Error
-- Please use this in preference to errorBox. If possible I'd like to
-- port all calls to errorBox to this and cease to export errorBox.
showErrorBox :: ( DomBuilder t m
               , MonadFix m
               , PostBuild t m
               , MonadHold t m
               ) => Event t Error -> m ()
showErrorBox trig = mdo
  currentErrorDyn <- holdDyn Nothing $ leftmost
    [ Just <$> (T.pack . show <$> trig)
    , Nothing <$ dismissEv ]
  dismissEv <- errorBox currentErrorDyn
  pure ()


-- | An MDC-themed dialog widget.
mdcDialog
    :: (DomBuilder t m, PostBuild t m)
    => Dynamic t Bool
    -- ^ A dynamic of whether the dialog is visible or not.
    -> Bool
    -- ^ A flag to make this modal show full-screen (True).
    -> m title
    -- ^ A widget for the dialog title.
    -> m content
    -- ^ A widget for the dialog content.
    -> m actions
    -- ^ A widget for any actions (eg, "ok" / "cancel" buttons) the dialog
    -- has.
    -> m (title, content, actions, Element EventResult (DomBuilderSpace m ) t)
    -- ^ A widget with the results of the components.
mdcDialog visible fullscreen title content actions =
    elDynAttr "div" topLevelDynAttrs $ do
      divClass "mdc-dialog__container" $ do
        divClass "mdc-dialog__surface" $ do
          (titleValue, closeButton) <- divClass "mdc-dialog__header" $ do
            titleValue <- elClass "h2" "mdc-dialog__title" title
            (closeButton, _) <- elAttr' "button" ("type" =: "button" <> "class" =: "mdc-icon-button material-icons mdc-dialog__close" <> "data-mdc-dialog-action" =: "close") $
              text "close"
            pure (titleValue, closeButton)

          contentValue <- divClass "mdc-dialog__content" content
          actionsValue <- divClass "mdc-dialog__actions" actions
          pure (titleValue, contentValue, actionsValue, closeButton)
      <* divClass "mdc-dialog__scrim" blank

    where
      topLevelDynAttrs = attributes' fullscreen <$> visible

      attributes' :: Bool -> Bool -> Map.Map T.Text T.Text
      -- Auto-initialize in "closed" state, then change class to
      -- "mdc-dialog--open" to show the dialog in a modal fashion (with an
      -- overlay temporarily disabling all other elements).
      attributes' True True = commonAttrs <> "class" =: "mdc-dialog mdc-dialog--open mdc-dialog--fullscreen"
      attributes' False True = commonAttrs <> "class" =: "mdc-dialog mdc-dialog--open"
      attributes' _ False = commonAttrs <> "class" =: "mdc-dialog mdc-dialog--closed"

      commonAttrs = "data-mdc-auto-init" =: "MDCDialog"


-- | An MDC-themed text field widget.
mdcText
    :: DomBuilder t m
    => m a
    -- ^ A widget of the unstyled text.
    -> m a
    -- ^ The text field widget with the result of the wrapped widget.
mdcText = elAttr "label" attrs
  where
    attrs
        = "class" =: "mdc-text-field mdc-text-field--filled"
        <> "data-mdc-auto-init" =: "MDCTextField"

mdcTextArea :: (DomBuilder t m) => m a -> m a
mdcTextArea inner = divClass "text-field-row text-field-row-fullwidth" $
    divClass "text-field-container" $ do
        result <- elAttr "div" attrs inner
        elClass "div" "mdc-text-field-helper-line" $
            elClass "p" "mdc-text-field-helper-text mdc-text-field-helper-text--persistent" $
                text "Enter Tahoe-LAFS storage grid configuration or leave empty to use the default"
        pure result
  where
    attrs =
        "class" =: "mdc-text-field text-field mdc-text-field--fullwidth mdc-text-field--no-label mdc-text-field--textarea"
            <> "data-mdc-auto-init" =: "MDCTextField"

-- | An MDC-themed button.
mdcButton
    :: DomBuilder t m
    => T.Text
    -- ^ The text on the button.
    -> m a
    -- ^ A widget that will be made a child of the button.
    -> m (Element EventResult (DomBuilderSpace m) t, a)
    -- ^ The button element and the result of the child widget.
mdcButton label =
  elAttr' "button" ("type" =: "button" <> "class" =: "mdc-button mdc-dialog__button" <> "data-mdc-dialog-action" =: label)


-- | Creates an MDC-themed button with dynamically controlled visibility.
mdcButtonDynEnabled ::
  (DomBuilder t m, PostBuild t m) =>
  Dynamic t Bool ->
  T.Text ->
  m a ->
  m (Element EventResult (DomBuilderSpace m) t, a)
mdcButtonDynEnabled enabledDyn label = do
-- Create a dynamic set of attributes based on the 'enabledDyn' value.
    let dynAttrs = ffor enabledDyn $ \isEnabled ->
    -- Define the base attributes for the button.
            let baseAttrs =
                    "type" =: "button"
                        <> "class" =: "mdc-button mdc-dialog__button"
                        <> "data-mdc-dialog-action" =: label
                -- Add a 'style' attribute to hide the button if 'isEnabled' is False.
                attrsEnabled =
                    if isEnabled
                        then baseAttrs
                        else baseAttrs <> "disabled" =: ""
            in attrsEnabled
      -- Create the button element with the dynamic attributes.
    elDynAttr' "button" dynAttrs

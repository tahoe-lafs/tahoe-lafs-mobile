{-# LANGUAGE BlockArguments      #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE RecursiveDo         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Pages.MagicFolders (magicFolders) where


import           App
import           Common.Route
import           Control.Exception                            (SomeException)
import           Control.Monad.Fix                            (MonadFix)
import           Control.Monad.IO.Class
import qualified Data.Map.Strict                              as M
import           Data.Maybe                                   (fromMaybe)
import           Data.Text                                    (Text)
import           MagicFolder
import           Obelisk.Route.Frontend
import           Pages.MagicFoldersComponents.ConfigComponent
import           Pages.MagicFoldersComponents.Folders
import           Pages.Widgets                                (errorBox)
import           PerformEvent                                 (performEventInThread)
import           Reflex.Dom.Core
import           Static                                       (logo)
import           Structs
import           WormholeInvite                               (doWormholeInviteOrCapAdd)

magicFolders
    :: forall t m . ( PerformEvent t m
       , DomBuilder t m
       , PostBuild t m
       , MonadHold t m
       , MonadIO (Performable m)
       , MonadFix m
       , TriggerEvent t m
       , SetRoute t (R FrontendRoute) m
       , MonadSample t (Performable m)
       ) =>
         App t -> m ()
magicFolders App{..} = do
      rec
        (config, _) <- elAttr "header" ("class" =: "mdc-top-app-bar mdc-top-app-bar--fixed" <> "data-mdc-auto-init" =: "MDCTopAppBar") $
          divClass "mdc-top-app-bar__row" $ do
            elClass "section" "mdc-top-app-bar__section mdc-top-app-bar__section--align-start" $ do
              (logotop, _) <- elAttr' "button" ("class" =: "material-icons mdc-top-app-bar__navigation-icon mdc-icon-button tahoe-logo-button") $
                elAttr "img" ("src" =: logo) blank
              elClass "span" "mdc-top-app-bar__title" $
                text "Tahoe-LAFS"

              -- an easter-egg to get to the tech-demo page
              -- (three clicks on the logo)
              counterDyn <- foldDyn (+) 0 (1 <$ domEvent Mousedown logotop)
              let easterEggEvent = ffilter (>= (3 :: Int)) (updated counterDyn)
              setRoute $ (FrontendRoute_TechDemo :/ []) <$ easterEggEvent

            elAttr "section" ("class" =: "mdc-top-app-bar__section mdc-top-app-bar__section--align-end" <> "role" =: "toolbar") $ do
                elClass' "div" "" $ do
                  -- Settings button to open the server configuration dialog.
                  elAttr "button" ("class" =: "material-icons mdc-top-app-bar__action-item mdc-icon-button" <> "aria-label" =: "Settings") $ do
                    divClass "mdc-fab__ripple" blank
                    elClass "span" "mdc-fab__icon material-icons" $
                      text "settings"

        -- Create the server configuration dialog. It is visible when updateServersState is UpdatingServers.
        newGrid :: Event t (Either () Text)
                <- gridConfigDialog ((UpdatingGrid ==) <$> updateGridState) appGridDyn appUpdateGridTr

        -- Extract separate events for canceling and accepting server updates from the dialog result.
        let
            cancelGridUpdate :: Event t ()
            (cancelGridUpdate, _) = fanEither newGrid

        -- Events that trigger state changes for server updates. Clicking the settings button starts
        -- an update, while canceling the dialog sets the state back to NotUpdating.
        let serverUpdateGrid =
              [ UpdatingGrid <$ domEvent Click config
              , NotUpdating <$ cancelGridUpdate
              ]

        -- Dynamically track the server update state.
        updateGridState <- holdDyn NotUpdating (leftmost serverUpdateGrid)

        _ <- elClass' "main" "magic-folder-page mdc-top-app-bar--fixed-adjust" $ do
          rec
            indeterminateLinearProgress acceptInviteState

            elAttr "ul" ("class" =: "mdc-deprecated-list mdc-deprecated-list--avatar-list" <> "data-mdc-auto-init" =: "MDCList") $
                dyn_ $ mapM_ folderWidget . M.toList . fromMaybe M.empty <$> appFoldersDyn
                --dynText $ bool "Full" "Empty" . null <$> appFoldersDyn

            (addBtn, _) <- elAttr' "button" ("class" =: "mdc-fab" <> "aria-label" =: "Add" <> "data-mdc-auto-init" =: "MDCRipple") $ do
              divClass "mdc-fab__ripple" blank
              elClass "span" "mdc-fab__icon material-icons" $
                text "add"
            -- Start out in the UI state where we're not waiting for any
            -- folder invite information. We progress through the other
            -- states in a circular fashion (EnteringCode,
            -- ReceivingInvite, InviteFinished then back to NotAccepting)

            -- transitions:
            --   click add -> EnteringCode
            --   form submitted -> ReceivingInvite
            --   (IO happened: folder-list updated, or...) -> InviteFinished
            --   dismiss clicked -> NotAccepting

            submitEitherInviteCode :: Event t (Either () Text)
                                   <- receiveFolderInviteText $ (EnteringCode ==) <$> acceptInviteState
            dismissError <- errorBox $ errorText <$> acceptInviteState
            let
              cancelInvite :: Event t ()
              submitInviteCode :: Event t Text
              (cancelInvite, submitInviteCode) = fanEither submitEitherInviteCode

            inviteCompleted :: Event t (Either SomeException InviteMessage)
                            <- performEventInThread $ doWormholeInviteOrCapAdd <$> submitInviteCode

            let (inviteFailed, inviteSucceeded) = fanEither inviteCompleted
            _ <- performEventInThread $ appInviteTr <$> inviteSucceeded
            -- showErrorBox $ NoWormhole <$ inviteFailed

            let inviteEvents =
                  [ EnteringCode <$ domEvent Click addBtn
                  , ReceivingInvite <$ submitInviteCode
                  , stateForInviteFailure <$> inviteFailed
                  , InviteFinished <$ inviteSucceeded
                  , NotAccepting <$ dismissError
                  , NotAccepting <$ cancelInvite
                  ]

            acceptInviteState <- holdDyn NotAccepting (leftmost inviteEvents)

          pure addBtn

      el "script" $ do
        text "mdc.autoInit();"



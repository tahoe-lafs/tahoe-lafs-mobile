{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE RecursiveDo         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE TypeFamilies        #-}

-- | Definitions related to the application-wide model.

module App (App(..), initialApp) where

import           Control.Monad.Fix      (MonadFix)
import           Control.Monad.IO.Class (MonadIO, liftIO)
import qualified Data.ByteString        as B
import           Data.Void              (Void)
import           Text.Megaparsec.Error  (ParseErrorBundle)
--import qualified Data.Text              as T
import           Control.Exception      (try)
import           Data.Bifunctor         (bimap, second)
import           Data.Functor           (($>), (<&>))
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as M
import           Data.Text              (Text)
import qualified Data.Yaml              as Yaml
import           FrontendPaths          (getFilesDir)
import           MagicFolder            (NamedFolder (NamedFolder),
                                         loadFolderMap, loadGrid, readFolder)
import           Obelisk.Configs        (HasConfigs)
import           Pages.Widgets          (showErrorBox)
import           PerformEvent
import           Reflex.Dom.Core
import           Structs
import           Tahoe.Announcement     (Announcements (..))
import qualified Tahoe.Directory        as Directory
import qualified Tahoe.Directory        as TD
import           Tahoe.MagicFoldr       (FolderEntry, MagicFolder (..))
import           Text.Megaparsec        (parse)
import           Utils

-- | The combined Tahoe-LAFS Mobile changeable application state.
data App t = App
    { -- | The Tahoe-LAFS storage servers the application will use.
      appGridDyn      :: Dynamic t (Either Text Announcements)
    , -- | A function to update the server config and check for changes to custom config.
      appUpdateGridTr :: () -> IO ()
    , -- | The Folders
      appFoldersDyn   :: Dynamic t (Maybe Folders) -- Only Nothing for a moment after startup
    , -- | Trigger to change the folders
      appInviteTr     :: InviteMessage -> IO ()
    }

-- | Load the initial application state from local configuration.
initialApp :: forall t m.
    ( MonadHold t m
    , MonadFix m
    , PerformEvent t m
    , PostBuild t m
    , TriggerEvent t m
    , MonadIO (Performable m)
    , DomBuilder t m
    , HasConfigs (Performable m)
    ) => Event t ()
      -> m (App t)
initialApp startEv = mdo

      -- Event and trigger for updating the grid configuration
    (gridUpdateEv, appUpdateGridTr) <- newTriggerEvent
    reloadGridEv <- performEvent $ ffor (leftmost [startEv, gridUpdateEv]) $ const loadGrid
    appGridDyn <- holdDyn (Left "Loading grid ...") reloadGridEv

    -- Load folders
    eCapTextMapEv         :: Event t (Either Error (Map FolderName Text))
                          <- performEvent (startEv $> loadFolderMap)
    let (no, yes)         :: (Event t Error, Event t (Map FolderName Text))
                           = fanEither eCapTextMapEv
    _                     <- showErrorBox no
    let initFoldersEv     :: Event t Folders -- TODO: Don't silently dump the errors with mapMaybe below
                           = yes <&&> parse TD.pReadSDMF "magic-folder collective capability"
                                  <&>  M.mapMaybe (either (const Nothing) (Just . (,Downloading Nothing)))
        makeInitFoldersEv :: Event t (Maybe Folders -> Maybe Folders)
                           = initFoldersEv <&> const . Just

    initFoldersDyn             <- holdDyn M.empty initFoldersEv
    let initFoldersWithGridDyn :: Dynamic t (Either Text Announcements, Folders)
                                = zipDyn appGridDyn initFoldersDyn

    -- On startup and grid change, renew everything non-destructively
    kickEverything :: Event t (Folders -> Folders) <- performEventsInThreads $
        updated initFoldersWithGridDyn <&> \(eGrid, folders) -> do
            case eGrid of
                Left _ -> []
                Right ann@(Announcements _) -> f:fs
                    where
                    f = pure allDownloading -- Show hour glasses while stuff is loading
                    fs = folders & M.toList <&> \(name, (capa, _phase)) -> do
                        let magicFolder = MagicFolder capa Nothing
                        let folder = NamedFolder name magicFolder
                        res <- readFolder ann folder
                        pure $ nonDestructivelyUpdateFolder name res

    -- Invites
    ( inviteEv :: Event t InviteMessage, appInviteTr ) <- newTriggerEvent
    let parsedInviteE   :: Event t (Either Error (Text, (ReadCap, Text))) -- (folder name, (parsed cap, original text of cap for saving))
                         = inviteEv <&> parseInvite
        (noPrs, parsed) :: (Event t Error, Event t (Text, (ReadCap, Text)))
                         = fanEither parsedInviteE
    _                   <- showErrorBox noPrs
    _                   <- performEvent_ $ parsed <&> second snd <&> liftIO . saveNewFolder
    let parsedInvite    :: Event t (Text, ReadCap)
                         = parsed <&> second fst
        insertNewFolder :: Event t (Folders -> Folders)
                         = parsedInvite <&> insertDownloadingFolder
        capAndGrid      :: Event t (Either Text Announcements, (Text, ReadCap))
                         = attach (current appGridDyn) parsedInvite

    downloadOneFolder :: Event t (Folders -> Folders) <- performEventInThread $ capAndGrid <&> \(eGrid, (name, cap)) -> do
        case eGrid of
            Left _ -> pure id
            Right ann@(Announcements _) -> do
                let magicFolder = MagicFolder cap Nothing
                let folder = NamedFolder name magicFolder
                res <- readFolder ann folder
                pure $ nonDestructivelyUpdateFolder name res

    let allFolderUpdates :: Event t (Maybe Folders -> Maybe Folders)
                          = leftmost [          makeInitFoldersEv
                                     , fmap <$> kickEverything
                                     , fmap <$> insertNewFolder
                                     , fmap <$> downloadOneFolder
                                     ]
    appFoldersDyn <- foldDyn ($) Nothing allFolderUpdates
    pure $ App{..}


parseInvite :: InviteMessage -> Either Error (Text, (ReadCap,Text))
parseInvite invite =
    let eParsed :: Either (ParseErrorBundle Text Void) ReadCap
           = parse Directory.pReadSDMF "magic-folder collective capability" $ collectiveReadCap invite
     in bimap CantParseInvite ((folderName invite,) . (,collectiveReadCap invite)) eParsed

-- When we just got a new folder, insert it into the map in downloading state
insertDownloadingFolder :: (Text,ReadCap) -> Folders -> Folders
insertDownloadingFolder (name,cap) = M.insert name (cap,Downloading Nothing)

nonDestructivelyUpdateFolder :: FolderName -> Either Error FolderEntry -> Folders -> Folders
nonDestructivelyUpdateFolder name eFE = M.adjust (second (`ndu` eFE)) name
    where
    ndu :: FolderPhase -> Either Error FolderEntry -> FolderPhase -- old then new
    -- If both good, use old
    ndu old@(Downloaded (Right _)) (Right _)  = old
    -- If only new is good, use it
    ndu _old                       (Right fe) = Downloaded (Right fe)
    -- If only old is good, keep but warn
    ndu (Downloaded (Right c))     _new       = Disappeared c
    -- If remembered something, keep it
    ndu (Downloading (Just c))     _new       = Disappeared c
    ndu (Disappeared c)            _new       = Disappeared c
    -- If both bad, use new
    ndu _old (Left e)                         = Downloaded (Left e)

-- | Set every folder to show an hour glass, with or without a loaded folder listing
allDownloading :: Folders -> Folders
allDownloading = M.map $ second beDownloading
    where
    beDownloading :: FolderPhase -> FolderPhase
    beDownloading (Downloaded (Right f)) = Downloading (Just f)
    beDownloading (Disappeared f)        = Downloading (Just f)
    beDownloading (Downloading (Just f)) = Downloading (Just f)
    beDownloading _                      = Downloading Nothing

-- Edits the folders file to add a folder just invited
saveNewFolder :: (Text, Text) -> IO ()
saveNewFolder (name, capa) = do
   Just fd <- getFilesDir -- partial, but only deliberate sabotage could get us here if this file were broken
   let fname = fd <> "/magicfolders.yaml"
   raw_existing <- try $ B.readFile fname
   case raw_existing of
     Right raw_data ->
       case Yaml.decodeEither' raw_data of
         Right existing -> do
           -- todo: handle conflicts
           let new_folders = M.insert name capa existing
           Yaml.encodeFile fname new_folders

         Left bad -> error $ show bad
     -- duplicate-looking, but refactor this out later
     Left (_ :: IOError) -> do
       let new_folders = M.insert name capa mempty
       Yaml.encodeFile fname new_folders


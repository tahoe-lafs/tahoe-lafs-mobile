{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Controller where

import           App                    (App (..))
import           Common.Route           (FolderLocation, FrontendRoute (..))
import           Control.Monad.Fix      (MonadFix)
import           Control.Monad.IO.Class (MonadIO)
import           Data.Bifunctor         (first)
import qualified Data.Map.Strict        as M
import qualified Data.Map.Strict        as Map
import           Data.Maybe             (fromMaybe)
import qualified Data.Text              as T
import           MagicFolder            (EntryName (..))
import           Obelisk.Route.Frontend
import           Pages.FileBrowser      (fileBrowser)
import           Pages.FirstRun         (firstRunPage)
import           Pages.MagicFolders     (magicFolders)
import           Pages.TechDemo         (techDemoPage)
import           Reflex.Dom.Core
import           Structs
import           Tahoe.MagicFoldr       (FolderEntry (..))


foldersToRoute :: ( PerformEvent t m
    , MonadIO (Performable m)
    , DomBuilder t m
    , SetRoute t (R FrontendRoute) m
    , PostBuild t m
    , MonadHold t m
    , MonadFix m
    , TriggerEvent t m
    , MonadSample t (Performable m))
    => App t -> Maybe Folders -> RoutedT t a m ()
foldersToRoute _ Nothing = blank
foldersToRoute app (Just folders) = if null folders then firstRunPage else magicFolders app

pageFromRoute ::
    ( PerformEvent t m
    , MonadIO (Performable m)
    , DomBuilder t m
    , SetRoute t (R FrontendRoute) m
    , SetRoute t (R FrontendRoute) (Client m)
    , PostBuild t m
    , Prerender t m
    , MonadHold t m
    , MonadFix m
    , TriggerEvent t m
    , MonadSample t (Performable m))
    => App t
    -> FrontendRoute a
    -> RoutedT t a m ()

pageFromRoute app FrontendRoute_Main =
    dyn_ (foldersToRoute app <$> appFoldersDyn app) -- This feels a bit dodgy

pageFromRoute app FrontendRoute_MagicFolders = magicFolders app

pageFromRoute app FrontendRoute_FileBrowser = do
    r <- askRoute
    dyn_ $ uncurry (fileBrowserIfYouCan app) <$> r <*> (fromMaybe M.empty <$> appFoldersDyn app)

pageFromRoute app FrontendRoute_TechDemo = do
    r <- askRoute
    techDemoPage app r


type FileBrowser t m =
  ( MonadHold t m
  , MonadIO (Performable m)
  , PerformEvent t m
  , TriggerEvent t m
  , PostBuild t m
  , DomBuilder t m
  , MonadFix m
  , SetRoute t (R FrontendRoute) m
  , Routed t FolderLocation m
  )

-- | Try to make a file browser widget.
fileBrowserIfYouCan
    :: FileBrowser t m
    => App t
    -- ^ Global application state!
    -> T.Text
    -- ^ The _name_ of the magic-folder to browse.
    -> [T.Text]
    -- ^ The path to the directory in the folder to browse.
    -> Folders
    -- ^ The magic-folders we know about.
    -> m ()
fileBrowserIfYouCan app fName path foldersDyn =
    let mFolder :: Maybe (ReadCap, FolderPhase) = M.lookup fName foldersDyn
     in case mFolder of
        Nothing -> text "Folder not found"
        Just (_, Downloadable )         -> text "Haven't attempted to download folder"
        Just (_, Downloading Nothing)   -> text "Still downloading folder"
        Just (_, Downloading (Just fe)) -> fileBrowser app (listWhichDir path fe)
        Just (_, Downloaded (Left e))   -> text $ "Error downloading folder: " <> T.pack (show e)
        Just (_, Downloaded (Right fe)) -> fileBrowser app (listWhichDir path fe)
        Just (_, Disappeared fe)        -> fileBrowser app (listWhichDir path fe)

-- | List the contents of a directory arbitrarily deeply nested beneath some
-- root.
listWhichDir
    :: [T.Text]
    -- ^ The path segments identifying the directory to list.
    -> FolderEntry
    -- ^ The root directory from which to start.
    -> [(EntryName, FolderEntry)]
    -- ^ The direct children of the selected directory.
listWhichDir [] rootEntry@(Directory _) = listThisDir rootEntry
listWhichDir (next:rest) (Directory dc) = concatMap (listWhichDir rest) (Map.lookup next dc)
-- Just rely on listThisDir to produce an error for this case.
listWhichDir _ entry = listThisDir entry

-- | Given a Directory, return the direct children annotated with their
-- basenames.
-- XXX Move this into some failable context.
listThisDir :: FolderEntry -> [(EntryName, FolderEntry)]
listThisDir (Directory dc) =  first EntryName <$> Map.assocs dc
listThisDir _ = [(EntryName "the system has failed, sorry, please file a bug report", defaultFolderEntry)]

-- | An empty directory.
defaultFolderEntry :: FolderEntry
defaultFolderEntry = Directory mempty


{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module PerformEvent where

import           Control.Concurrent     (forkIO)
import           Control.Exception      (Exception, try)
import           Control.Monad          (forM_, void)
import           Control.Monad.IO.Class (MonadIO, liftIO)
import           Reflex.Dom.Core        (Event, PerformEvent, Performable,
                                         TriggerEvent, ffor, performEventAsync)

-- | Execute IO actions from an event in a separate thread and produce an
-- Event that triggers with their results.  Ordering is not guaranteed.
performEventInThreadEx
    :: ( TriggerEvent t m
       , PerformEvent t m
       , MonadIO (Performable m)
       , Exception e
       )
    => Event t (IO a)
    -> m (Event t (Either e a))
performEventInThreadEx evt =
  performEventAsync $ ffor evt $
    \action callback -> liftIO . void . forkIO $ try action >>= callback

-- | Like performEventInThreadEx but without exceptions
performEventInThread
    :: ( TriggerEvent t m , PerformEvent t m , MonadIO (Performable m))
    => Event t (IO a)
    -> m (Event t a)
performEventInThread evt =
  performEventAsync $ ffor evt $
    \action callback -> liftIO . void . forkIO $ action >>= callback

-- | Like performEventInThread but running multiple actions concurrently
-- | and posting all their results to the returned event.
performEventsInThreads
    :: ( TriggerEvent t m , PerformEvent t m , MonadIO (Performable m))
    => Event t [IO a]
    -> m (Event t a)
performEventsInThreads evt = do
  performEventAsync $ ffor evt $
    \actions callback -> forM_ actions $
      \action -> liftIO . void . forkIO $ action >>= callback


{ mkDerivation, array, async, auto-update, base, bsb-http-chunked
, bytestring, case-insensitive, containers, directory, gauge
, ghc-prim, hashable, hspec, http-client, http-date, http-types
, http2, HUnit, iproute, network, process, QuickCheck
, simple-sendfile, stm, streaming-commons, text, time, time-manager
, unix, unix-compat, unliftio, vault, wai, word8, x509, lib
}:
mkDerivation {
  pname = "warp";
  version = "3.3.17";
  sha256 = "0v54ca3wpa79gdyiikwhbv9h8b5vr3d60piq3ndb2v7s7fi1qpm0";
  libraryHaskellDepends = [
    array auto-update base bsb-http-chunked bytestring case-insensitive
    containers ghc-prim hashable http-date http-types http2 iproute
    network simple-sendfile stm streaming-commons text time-manager
    unix unix-compat unliftio vault wai word8 x509
  ];
  testHaskellDepends = [
    array async auto-update base bsb-http-chunked bytestring
    case-insensitive containers directory ghc-prim hashable hspec
    http-client http-date http-types http2 HUnit iproute network
    process QuickCheck simple-sendfile stm streaming-commons text time
    time-manager unix unix-compat unliftio vault wai word8 x509
  ];
  benchmarkHaskellDepends = [
    auto-update base bytestring containers gauge hashable http-date
    http-types network time-manager unix unix-compat unliftio x509
  ];
  description = "A fast, light-weight web server for WAI applications";
  license = lib.licenses.mit;
}
